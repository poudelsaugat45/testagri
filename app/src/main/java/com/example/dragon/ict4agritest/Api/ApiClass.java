package com.example.dragon.ict4agritest.Api;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.Arrays;

import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClass {
    private OkHttpClient okHttpClient = new OkHttpClient()
            .newBuilder()
            .connectionSpecs(Arrays.asList(ConnectionSpec.MODERN_TLS, ConnectionSpec.CLEARTEXT))
            .followRedirects(false)
            .build();

    private Gson gson = new Gson().newBuilder().setLenient().create();
    public Retrofit getRetrofit(){
        return new Retrofit.Builder()
                .baseUrl("https://admin.ict4agri.com/api/v2/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

}