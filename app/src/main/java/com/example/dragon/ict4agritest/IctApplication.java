package com.example.dragon.ict4agritest;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

public class IctApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }
}
