package com.example.dragon.ict4agritest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.dragon.ict4agritest.Adapters.MyPostsAdapter;
import com.facebook.drawee.view.SimpleDraweeView;

public class UserProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        RecyclerView recyclerView = findViewById(R.id.posts_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(new MyPostsAdapter(getApplicationContext()));


        String imageurl = "https://rpgamer.com/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg";

        final SharedPreferences sharedPreferences = getSharedPreferences("LOGIN_DETAILS", MODE_PRIVATE);
        if (sharedPreferences.getString("IMAGE", null)!=null){
            imageurl = sharedPreferences.getString("IMAGE", null);
        }
        SimpleDraweeView userImage = findViewById(R.id.userImage);
        userImage.setImageURI(Uri.parse(imageurl));
        TextView nameText = findViewById(R.id.nameText);
        TextView addressText = findViewById(R.id.addressText);
        TextView phoneText = findViewById(R.id.phoneText);
        Button logoutButton = findViewById(R.id.logoutButton);

        nameText.setText(sharedPreferences.getString("FULL_NAME", "NULL"));
        addressText.setText(sharedPreferences.getString("LOCATION", "NULL"));
        phoneText.setText(sharedPreferences.getString("USERNAME", "NULL"));

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences.edit().clear().apply();
                Intent intent = new Intent(UserProfile.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }
}
