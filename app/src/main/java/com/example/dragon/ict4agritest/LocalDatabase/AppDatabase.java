package com.example.dragon.ict4agritest.LocalDatabase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.dragon.ict4agritest.Models.PostData;

@Database(entities = {PostData.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract PostDataDAO postDataDAO();
    private static volatile AppDatabase INSTANCE;

    public static AppDatabase getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (AppDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context, AppDatabase.class, "database").build();
                }
            }
        }
        return INSTANCE;
    }

}

