package com.example.dragon.ict4agritest;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.dragon.ict4agritest.Adapters.MyPostsAdapter;
import com.example.dragon.ict4agritest.Api.ApiClass;
import com.example.dragon.ict4agritest.Api.ApiService;
import com.example.dragon.ict4agritest.Models.PostBase;
import com.example.dragon.ict4agritest.Models.PostData;
import com.example.dragon.ict4agritest.Utils.ConnectivityUtils;
import com.example.dragon.ict4agritest.Utils.EndlessRecyclerViewScrollListener;
import com.example.dragon.ict4agritest.viewmodels.MainActivityViewModel;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    private EndlessRecyclerViewScrollListener endlessScrollListner;
    MyPostsAdapter myPostsAdapter;
    SharedPreferences sharedPreferences;
    String apiKey = "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    ProgressDialog progressDialog;
    MainActivityViewModel mainActivityViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences("LOGIN_DETAILS", MODE_PRIVATE);
        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching data from server...");
        progressDialog.show();
        RecyclerView posts_recycler_view = findViewById(R.id.posts_recycler_view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        posts_recycler_view.setLayoutManager(linearLayoutManager);

        myPostsAdapter = new MyPostsAdapter(getApplicationContext());

        posts_recycler_view.setAdapter(myPostsAdapter);

        loadMoreData(0, 20);
        mainActivityViewModel.getmPostData().observe(this, new Observer<List<PostData>>() {
            @Override
            public void onChanged(@Nullable List<PostData> postData) {
                myPostsAdapter.setPostDataList(postData);
            }
        });
        endlessScrollListner = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                //loadMoreData(page, 20);
            }
        };
        posts_recycler_view.addOnScrollListener(endlessScrollListner);
    }

    public void loadMoreData(final int start, final int length) {

        Log.i("MAINACTIVITY_LENGTH", (start*20)+" "+length);
        ApiClass apiClass = new ApiClass();
        ApiService apiService = apiClass.getRetrofit().create(ApiService.class);
        if (ConnectivityUtils.isConnected(getApplicationContext())) {
            compositeDisposable.add(apiService.postsByPage(apiKey, start*20, length, Integer.parseInt(sharedPreferences.getString("ID", "0")))
                    .subscribeOn(Schedulers.io())
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .doOnError(new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            if (progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                            Log.i("API_ERROR_MAINACTIVITY", throwable.getLocalizedMessage() );
                        }
                    })
                    .subscribe(new Consumer<PostBase>() {
                        @Override
                        public void accept(PostBase postBases) {
                            if (progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                            mainActivityViewModel.insertPostData(postBases.getData().get(0));
                        }
                    }));
        }
        else {
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.accountMenu:
                Intent intent = new Intent(MainActivity.this, UserProfile.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*loadMoreData(0, 30);*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

}

