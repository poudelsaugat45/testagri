package com.example.dragon.ict4agritest.Adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dragon.ict4agritest.Models.CommentsData;
import com.example.dragon.ict4agritest.R;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {
    private Context mContext;
    private List<CommentsData> commentsDataArrayList;

    public CommentsAdapter(Context context){
        mContext = context;
    }

    @NonNull
    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.comment_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsAdapter.ViewHolder viewHolder, int i) {
        viewHolder.commentText.setText(commentsDataArrayList.get(viewHolder.getAdapterPosition()).getComment());
        viewHolder.nameText.setText(commentsDataArrayList.get(viewHolder.getAdapterPosition()).getCommentedBy());
        String imageurl = "https://rpgamer.com/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg";
        Uri uri = Uri.parse(imageurl);
        viewHolder.simpleDraweeView.setImageURI(uri);
    }

    @Override
    public int getItemCount() {
        if (commentsDataArrayList== null){
            return 0;
        }else
            return commentsDataArrayList.size();
    }

    public void setCommentsDataArrayList(List<CommentsData> mCommentsDataArrayList) {
/*
        if (commentsDataArrayList==null){
*/
            Log.i("COMMENTSLIST_ACTIVITY_N", mCommentsDataArrayList.size()+"");
            commentsDataArrayList = mCommentsDataArrayList;
            notifyDataSetChanged();
        /*}
        else {
            commentsDataArrayList.addAll(mCommentsDataArrayList);
            Log.i("COMMENTSLIST_ACTIVITY", mCommentsDataArrayList.size()+"");
            notifyDataSetChanged();
        }*/
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView nameText, timeText, commentText;
        SimpleDraweeView simpleDraweeView;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameText = itemView.findViewById(R.id.nameText);
            timeText = itemView.findViewById(R.id.timeText);
            commentText = itemView.findViewById(R.id.commentText);
            simpleDraweeView = itemView.findViewById(R.id.simpleDraweeView);
        }
    }
}
