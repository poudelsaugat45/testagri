package com.example.dragon.ict4agritest.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.dragon.ict4agritest.LocalDatabase.Repos.PostDataRepo;
import com.example.dragon.ict4agritest.Models.PostData;

import java.util.List;

public class MainActivityViewModel extends AndroidViewModel {
    private PostDataRepo postDataRepo;
    private LiveData<List<PostData>> mPostData;
    public MainActivityViewModel(@NonNull Application application) {
        super(application);
        postDataRepo = new PostDataRepo(application);
        mPostData = postDataRepo.getPostDataList();
    }

    public LiveData<List<PostData>> getmPostData(){
        return mPostData;
    }

    public void insertPostData(PostData postData){
        postDataRepo.insert(postData);
    }
}
