package com.example.dragon.ict4agritest.Api;

import com.example.dragon.ict4agritest.Models.CommentsResponse;
import com.example.dragon.ict4agritest.Models.LikeResponse;
import com.example.dragon.ict4agritest.Models.LoginResponse;
import com.example.dragon.ict4agritest.Models.PostBase;
import com.example.dragon.ict4agritest.Models.RegisterResponse;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("forum/questions")
    Single<PostBase> postsByPage(@Query("apikey") String apiKey,
                                 @Query("start") int startInt,
                                 @Query("length") int lengthInt,
                                 @Query("user_id") int userId
    );

    @GET("forum/comments/{id}")
    Single<CommentsResponse> commentsByPage(@Path("id") int post_id,
                                            @Query("apikey") String apiKey,
                                            @Query("start") int startInt,
                                            @Query("length") int lengthInt,
                                            @Query("user_id") int userId
    );
    @POST("user/login")
    Single<LoginResponse> login(@Query("apikey") String apiKey,
                                @Query("username") String username,
                                @Query("password") String password
    );

    @FormUrlEncoded
    @POST("user/register")
    Single<RegisterResponse> register(@Query("apikey") String apiKey ,
                                      @Field(value = "username", encoded = true) String username,
                                      @Field(value = "name", encoded = true) String name,
                                      @Field(value = "password", encoded = true) String password
    );

    @POST("question/{question_id}/like")
    Single<LikeResponse> like(@Path("question_id") int question_id,
                              @Query("apikey") String apiKey ,
                              @Query("user_id") int userId
                          );

    @FormUrlEncoded
    @POST("question/{question_id}/comment")
    Single<ResponseBody> comment(@Path("question_id") int question_id,
                          @Query("apikey") String apiKey ,
                          @Field("user_id") int userId,
                          @Field("comment") String comment
    );
}