package com.example.dragon.ict4agritest;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dragon.ict4agritest.Api.ApiClass;
import com.example.dragon.ict4agritest.Api.ApiService;
import com.example.dragon.ict4agritest.Models.LoginResponse;
import com.example.dragon.ict4agritest.Utils.ConnectivityUtils;
import com.google.gson.JsonIOException;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class LoginActivity extends AppCompatActivity{
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    private String username, password;
    ProgressDialog progressDialog;
    String apiKey = "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ApiClass apiClass = new ApiClass();
        final ApiService apiService = apiClass.getRetrofit().create(ApiService.class);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        sharedPreferences = getSharedPreferences("LOGIN_DETAILS", MODE_PRIVATE);
        if (sharedPreferences.getString("USERNAME", null) != null){
            startMainActivity();
        }

        final EditText usernameText = findViewById(R.id.usernameText);
        final EditText passwordText = findViewById(R.id.passwordText);
        Button loginButton = findViewById(R.id.loginButton);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Logging in..");
                progressDialog.show();
                username = usernameText.getText().toString();
                password = passwordText.getText().toString();
                if (ConnectivityUtils.isConnected(getApplicationContext())) {
                    compositeDisposable.add(apiService.login(apiKey, username, password)
                            .subscribeOn(Schedulers.io())
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .doOnError(new Consumer<Throwable>() {
                                @Override
                                public void accept(Throwable throwable) throws Exception {
                                    Log.i("API_ERROR_LOGINACTIVITY", throwable.getLocalizedMessage() );
                                    if (progressDialog.isShowing()){
                                        progressDialog.dismiss();
                                    }
                                }
                            })
                    .subscribe(new Consumer<LoginResponse>() {
                        @Override
                        public void accept(final LoginResponse loginResponse) throws Exception {
                            if (progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }

                            if (null != loginResponse.getData().get(0).getStatus()){
                                Log.i("RESPONSE_ERROR", username+" "+password);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), loginResponse.getData().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                            else {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("FULL_NAME", loginResponse.getData().get(0).getFullname());
                                editor.putString("ID", loginResponse.getData().get(0).getId());
                                editor.putString("USER_TYPE", loginResponse.getData().get(0).getUserType());
                                editor.putString("LOCATION", loginResponse.getData().get(0).getLat()+","+loginResponse.getData().get(0).getLng());
                                editor.putString("USERNAME", loginResponse.getData().get(0).getUsername());
                                editor.apply();
                                startMainActivity();
                            }

                        }
                    }));
                }
                else {

                }
            }
        });

        TextView signupText = findViewById(R.id.signupText);
        signupText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signupIntent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(signupIntent);
                finish();
            }
        });
    }

    void startMainActivity(){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}
