package com.example.dragon.ict4agritest;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dragon.ict4agritest.Api.ApiClass;
import com.example.dragon.ict4agritest.Api.ApiService;
import com.example.dragon.ict4agritest.Models.RegisterResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class SignupActivity extends AppCompatActivity {
    ApiClass apiClass;
    ApiService apiService;
    String apiKey = "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        apiClass = new ApiClass();
        apiService = apiClass.getRetrofit().create(ApiService.class);

        sharedPreferences = getSharedPreferences("LOGIN_DETAILS", MODE_PRIVATE);
        final EditText fullnameText = findViewById(R.id.fullnameText);
        final EditText usernameText = findViewById(R.id.usernameText);
        final EditText passwordText = findViewById(R.id.passwordText);

        Button signupButton = findViewById(R.id.signupButton);

        TextView loginText = findViewById(R.id.loginText);
        loginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signupIntent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(signupIntent);
                finish();
            }
        });

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this);
                progressDialog.setMessage("Signing up, please wait..");
                progressDialog.show();
                String fullName = fullnameText.getText().toString();
                String userName = usernameText.getText().toString();
                String password = passwordText.getText().toString();

                apiService.register(apiKey, userName, fullName, password)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError(new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                if (progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }
                                Log.i("SIGNUP_ACTIVITY_ERR", throwable.getLocalizedMessage());
                            }
                        })
                        .subscribe(new Consumer<RegisterResponse>() {
                            @Override
                            public void accept(RegisterResponse registerResponse) throws Exception {
                                if (progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }
                                if (registerResponse.getStatus().equals("1")){
                                    Toast.makeText(getApplicationContext(), registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString("FULL_NAME", registerResponse.getData().get(0).getName());
                                    editor.putString("ID", registerResponse.getData().get(0).getId());
                                    editor.putString("USER_TYPE", registerResponse.getData().get(0).getUserType());
                                    editor.putString("LOCATION", registerResponse.getData().get(0).getLocation());
                                    editor.putString("USERNAME", registerResponse.getData().get(0).getPhone());
                                    editor.putString("IMAGE", registerResponse.getData().get(0).getImage());
                                    editor.apply();
                                    startMainActivity();
                                }
                                else {
                                    Toast.makeText(getApplicationContext(), registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
    }
    void startMainActivity(){
        Intent intent = new Intent(SignupActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
