package com.example.dragon.ict4agritest.LocalDatabase;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.dragon.ict4agritest.Models.PostData;

import java.util.List;

@Dao
public interface PostDataDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PostData postData);

    @Query("SELECT * FROM POST_DATA ORDER BY ID DESC")
    LiveData<List<PostData>> getAllPosts();
}
