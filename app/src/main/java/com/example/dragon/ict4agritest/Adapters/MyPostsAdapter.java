package com.example.dragon.ict4agritest.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dragon.ict4agritest.Api.ApiClass;
import com.example.dragon.ict4agritest.Api.ApiService;
import com.example.dragon.ict4agritest.CommentActivity;
import com.example.dragon.ict4agritest.Models.LikeResponse;
import com.example.dragon.ict4agritest.Models.PostData;
import com.example.dragon.ict4agritest.R;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.MODE_PRIVATE;

public class MyPostsAdapter extends RecyclerView.Adapter<MyPostsAdapter.ViewHolder> {
    ApiClass apiClass;
    ApiService apiService;
    CompositeDisposable compositeDisposable;
    String apiKey = "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    SharedPreferences sharedPreferences;
    private Context mContext;
    private List<PostData> postDataList;

    public MyPostsAdapter(Context context) {
        mContext = context;
        apiClass = new ApiClass();
        apiService = apiClass.getRetrofit().create(ApiService.class);
        compositeDisposable = new CompositeDisposable();
        sharedPreferences = context.getSharedPreferences("LOGIN_DETAILS", MODE_PRIVATE);
    }

    @NonNull
    @Override
    public MyPostsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.user_post, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyPostsAdapter.ViewHolder viewHolder, int i) {
        int likes;
        final boolean[] liked = new boolean[1];

        if (postDataList.get(viewHolder.getAdapterPosition()).getImageUrl() != null){
            if (postDataList.get(viewHolder.getAdapterPosition()).getImageUrl().length()>0) {
                viewHolder.postImage.setVisibility(View.VISIBLE);
                viewHolder.postImage.setImageURI(Uri.parse(postDataList.get(viewHolder.getAdapterPosition()).getImageUrl()));
            }
            else {
                viewHolder.postImage.setVisibility(View.GONE);
            }
        }
        String imageurl;
        if (postDataList.get(viewHolder.getAdapterPosition()).getUserImage() == null) {
            imageurl = "https://rpgamer.com/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg";
        } else {
            imageurl = postDataList.get(viewHolder.getAdapterPosition()).getUserImage();
        }
        Uri imageUri = Uri.parse(imageurl);

        viewHolder.profileImage.setImageURI(imageUri);
        viewHolder.titleText.setText(postDataList.get(viewHolder.getAdapterPosition()).getQuestion());
        viewHolder.locationText.setText(postDataList.get(viewHolder.getAdapterPosition()).getDistrict());
        viewHolder.descriptionText.setText(postDataList.get(viewHolder.getAdapterPosition()).getDescription());
        viewHolder.nameText.setText(postDataList.get(viewHolder.getAdapterPosition()).getAskBy());
        viewHolder.likeButton.setText(String.format("%s Likes", postDataList.get(viewHolder.getAdapterPosition()).getLikeCount()));

        liked[0] = Boolean.parseBoolean(postDataList.get(viewHolder.getAdapterPosition()).getIsLiked());
        viewHolder.commentButton.setText(String.format("%s Comments", postDataList.get(viewHolder.getAdapterPosition()).getCommentCount()));
        viewHolder.commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CommentActivity.class);
                intent.putExtra("POSTDATA", postDataList.get(viewHolder.getAdapterPosition()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

        if (liked[0]) {
            viewHolder.likeButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_black_24dp, 0, 0, 0);
        }
        viewHolder.likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!liked[0]) {
                    compositeDisposable.add(apiService.like(Integer.parseInt(postDataList.get(viewHolder.getAdapterPosition()).getId()), apiKey, Integer.parseInt(sharedPreferences.getString("ID", "0")))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Consumer<LikeResponse>() {
                                @Override
                                public void accept(LikeResponse likeResponse) throws Exception {
                                    if (likeResponse.getData().get(0).getStatus().equals("1")) {
                                        postDataList.get(viewHolder.getAdapterPosition()).setIsLiked("true");
                                        postDataList.get(viewHolder.getAdapterPosition()).setLikeCount(""+(Integer.parseInt(postDataList.get(viewHolder.getAdapterPosition()).getLikeCount())+1));
                                        liked[0] = true;
                                        viewHolder.likeButton.setText(String.format("%d Likes", Integer.parseInt(postDataList.get(viewHolder.getAdapterPosition()).getLikeCount())));
                                        viewHolder.likeButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_black_24dp, 0, 0, 0);
                                        Toast.makeText(mContext, likeResponse.getData().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (postDataList == null) {
            return 0;
        } else {
            return postDataList.size();
        }
    }

    public void setPostDataList(List<PostData> postReceivedDataList) {
        if (postDataList == null) {
            this.postDataList = postReceivedDataList;
        } else {
            this.postDataList.addAll(postReceivedDataList);
        }
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleText, nameText, timeText, descriptionText, locationText;
        SimpleDraweeView profileImage, postImage;
        Button likeButton, commentButton;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleText = itemView.findViewById(R.id.titleText);
            nameText = itemView.findViewById(R.id.nameText);
            timeText = itemView.findViewById(R.id.timeText);
            descriptionText = itemView.findViewById(R.id.descriptionText);
            profileImage = itemView.findViewById(R.id.profileImage);
            likeButton = itemView.findViewById(R.id.likeButton);
            commentButton = itemView.findViewById(R.id.commentButton);
            locationText = itemView.findViewById(R.id.locationText);
            postImage = itemView.findViewById(R.id.postImage);
        }
    }
}
