package com.example.dragon.ict4agritest.LocalDatabase.Repos;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.dragon.ict4agritest.LocalDatabase.AppDatabase;
import com.example.dragon.ict4agritest.LocalDatabase.PostDataDAO;
import com.example.dragon.ict4agritest.Models.PostData;

import java.util.List;

public class PostDataRepo {
    private PostDataDAO postDataDAO;
    private LiveData<List<PostData>> postDataList;

    public PostDataRepo(Application application){
        AppDatabase appDatabase = AppDatabase.getDatabase(application);
        postDataDAO = appDatabase.postDataDAO();
        postDataList = postDataDAO.getAllPosts();
    }

    public LiveData<List<PostData>> getPostDataList(){
        return postDataList;
    }

    public void insert(PostData postData){
        new insertAsync(postDataDAO).execute(postData);

    }

    private static class insertAsync extends AsyncTask<PostData, Void, Void>{
        private PostDataDAO mAsyncTaskDao;
        insertAsync(PostDataDAO postDataDAO){
            mAsyncTaskDao = postDataDAO;
        }
        @Override
        protected Void doInBackground(PostData... postData) {
            mAsyncTaskDao.insert(postData[0]);
            return null;
        }
    }
}
