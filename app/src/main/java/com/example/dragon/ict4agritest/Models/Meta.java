package com.example.dragon.ict4agritest.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meta {

    @SerializedName("total_count")
    @Expose
    private Integer totalCount;
    @SerializedName("total_displayed")
    @Expose
    private Integer totalDisplayed;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getTotalDisplayed() {
        return totalDisplayed;
    }

    public void setTotalDisplayed(Integer totalDisplayed) {
        this.totalDisplayed = totalDisplayed;
    }

}