package com.example.dragon.ict4agritest;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dragon.ict4agritest.Adapters.CommentsAdapter;
import com.example.dragon.ict4agritest.Api.ApiClass;
import com.example.dragon.ict4agritest.Api.ApiService;
import com.example.dragon.ict4agritest.Models.CommentsResponse;
import com.example.dragon.ict4agritest.Models.LikeResponse;
import com.example.dragon.ict4agritest.Models.PostData;
import com.example.dragon.ict4agritest.Utils.ConnectivityUtils;
import com.facebook.drawee.view.SimpleDraweeView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class CommentActivity extends AppCompatActivity {
    TextView titleText, nameText, timeText, descriptionText, locationText;
    EditText commentEditText;

    SimpleDraweeView profileImage, postImage;
    Button likeButton, commentButton, commentSendButton, shareButton;
    ProgressDialog progressDialog;
    PostData postDataReceived;
    SharedPreferences sharedPreferences;
    CommentsAdapter commentsAdapter;
    String apiKey = "O_za1PLmQwpKktCNak2aV9bxP-SW7rrmBlIGDUcA";
    ApiClass apiClass;
    ApiService apiService;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_fragment);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading, please wait...");

        apiClass = new ApiClass();
        apiService = apiClass.getRetrofit().create(ApiService.class);
        commentsAdapter = new CommentsAdapter(getApplicationContext());

        progressDialog.show();
        sharedPreferences = getSharedPreferences("LOGIN_DETAILS", MODE_PRIVATE);

        titleText = findViewById(R.id.titleText);
        nameText = findViewById(R.id.nameText);
        timeText = findViewById(R.id.timeText);
        descriptionText = findViewById(R.id.descriptionText);
        profileImage = findViewById(R.id.profileImage);
        likeButton = findViewById(R.id.likeButton);
        commentButton = findViewById(R.id.commentButton);
        locationText = findViewById(R.id.locationText);
        commentEditText = findViewById(R.id.commentEditText);
        commentSendButton = findViewById(R.id.commentSendButton);
        postImage = findViewById(R.id.postImage);
        shareButton = findViewById(R.id.shareButton);

        if (getIntent().getSerializableExtra("POSTDATA") != null) {
            postDataReceived = (PostData) getIntent().getSerializableExtra("POSTDATA");
        }
        ;

        if (null != postDataReceived.getImageUrl()) {
            if (postDataReceived.getImageUrl().length() > 0) {
                postImage.setVisibility(View.VISIBLE);
                postImage.setImageURI(Uri.parse(postDataReceived.getImageUrl()));
            } else {
                postImage.setVisibility(View.GONE);
            }
        }
        //EXACT COPY OF THE RECEIVED DATA
        String imageurl;
        if (postDataReceived.getUserImage() == null) {
            imageurl = "https://rpgamer.com/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg";
        } else {
            imageurl = postDataReceived.getUserImage();
        }
        Uri imageUri = Uri.parse(imageurl);
        profileImage.setImageURI(imageUri);
        titleText.setText(postDataReceived.getQuestion());
        locationText.setText(postDataReceived.getDistrict());
        descriptionText.setText(postDataReceived.getDescription());
        nameText.setText(postDataReceived.getAskBy());
        likeButton.setText(String.format("%s Likes", postDataReceived.getLikeCount()));
        if (Boolean.parseBoolean(postDataReceived.getIsLiked())) {
            likeButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_black_24dp, 0, 0, 0);
            likeButton.setText(String.format("%s Liked", postDataReceived.getLikeCount()));
        }

        commentButton.setText(String.format("%s Comments", postDataReceived.getCommentCount()));

        RecyclerView commentsList = findViewById(R.id.commentsList);
        commentsList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        commentsList.setAdapter(commentsAdapter);

        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Boolean.parseBoolean(postDataReceived.getIsLiked())) {
                    compositeDisposable.add(apiService.like(Integer.parseInt(postDataReceived.getId()), apiKey, Integer.parseInt(sharedPreferences.getString("ID", "0")))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Consumer<LikeResponse>() {
                                @Override
                                public void accept(LikeResponse likeResponse) throws Exception {
                                    if (likeResponse.getData().get(0).getStatus().equals("1")) {
                                        likeButton.setText(String.format("%d Likes", Integer.parseInt(postDataReceived.getLikeCount()) + 1));
                                        likeButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_black_24dp, 0, 0, 0);
                                        Toast.makeText(getApplicationContext(), likeResponse.getData().get(0).getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }));
                }
            }
        });
        loadMoreData(0, 10);

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sharingText = postDataReceived.getQuestion()+"\n"+postDataReceived.getDescription();
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Ict for agri");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, sharingText);
                startActivity(Intent.createChooser(sharingIntent, "Share with"));
            }
        });

        commentSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                apiClass = new ApiClass();
                apiService = apiClass.getRetrofit().create(ApiService.class);
                String comment = commentEditText.getText().toString();
                if (commentEditText.getText().toString().trim().length() > 6) {
                    compositeDisposable.add(apiService.comment(Integer.parseInt(postDataReceived.getId()), apiKey, Integer.parseInt(sharedPreferences.getString("ID", "0")), comment)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnError(new Consumer<Throwable>() {
                                @Override
                                public void accept(Throwable throwable) throws Exception {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                    Log.i("COMMENTACTIVITY_ERROR", throwable.getLocalizedMessage());
                                }
                            })
                            .subscribe(new Consumer<ResponseBody>() {
                                @Override
                                public void accept(ResponseBody response) throws Exception {
                                    Log.i("COMMENTACTIVITY_RESP", response.string());
                                    commentEditText.setText("");
                                    Toast.makeText(getApplicationContext(), "Comment added successfully", Toast.LENGTH_SHORT).show();

                                    loadMoreData(0, 20);
                                }
                            }));
                } else {
                    Toast.makeText(getApplicationContext(), "Comment text must be more than 6 letters", Toast.LENGTH_SHORT).show();
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            }
        });
    }

    public void loadMoreData(final int start, final int length) {

        Log.i("COMMENT_ACTIVITY", start + " " + length);

        if (ConnectivityUtils.isConnected(getApplicationContext())) {
            compositeDisposable.add(apiService.commentsByPage(Integer.parseInt(postDataReceived.getId()), apiKey, 0, 10, Integer.parseInt(sharedPreferences.getString("ID", "0")))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<CommentsResponse>() {
                        @Override
                        public void accept(final CommentsResponse commentsResponse) throws Exception {
                            commentsAdapter.setCommentsDataArrayList(commentsResponse.getData());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                }
                            });
                        }
                    }));
        } else {
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}
