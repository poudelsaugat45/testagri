package com.example.dragon.ict4agritest.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LikeResponse {

    @SerializedName("data")
    @Expose
    private List<SimpleData> data = null;

    public List<SimpleData> getData() {
        return data;
    }

    public void setData(List<SimpleData> data) {
        this.data = data;
    }

}

